#!/usr/bin/env python

from os import path
from sys import argv
import argparse
import boto3

parser = argparse.ArgumentParser(description='A test program.')
parser.add_argument("--delete", metavar=("Old-Path", "New-Path"), help="Delete old parameters after copying", nargs=2)
parser.add_argument("--copy", metavar=("Old-Path", "New-Path"), help="Copy and preserve old parameters", nargs=2)
parser.add_argument("-s", "--silent", action='store_true', help="Run without printing parameter names")
args = parser.parse_args()

if (args.delete):
    OLD_PATH = args.delete[0]
    NEW_PATH = args.delete[1]
elif (args.copy):
    OLD_PATH = args.copy[0]
    NEW_PATH = args.copy[1]

SSM = boto3.client('ssm', region_name="ca-central-1")

def get_parameters_by_path(next_token = None):
    params = {
        'Path': OLD_PATH,
        'Recursive': True,
        'WithDecryption': True
    }
    if next_token is not None:
        params['NextToken'] = next_token
    return SSM.get_parameters_by_path(**params)

def parameters():
    next_token = None
    while True:
        response = get_parameters_by_path(next_token)
        parameters = response['Parameters']
        if len(parameters) == 0:
            break
        for parameter in parameters:
            yield parameter
        if 'NextToken' not in response:
            break
        next_token = response['NextToken']

def print_env_vars(parameter):
    env_name = path.basename(parameter['Name'])
    env_value = parameter['Value']
    env_type = parameter['Type']
    print("%s=\"%s\",%s" % (env_name, env_value, env_type))

def put_env_vars(parameter):
    new_env_name = NEW_PATH + path.basename(parameter['Name'])
    new_env_value = parameter['Value']
    new_env_type = parameter['Type']
    if not (args.silent): print("%s=\"%s\",%s" % (new_env_name, new_env_value, new_env_type))
    SSM.put_parameter(
        Name = new_env_name,
        Value = new_env_value,
        Type = new_env_type,
        Overwrite = True
    )

def delete_env_vars(parameter):
    delete_env_name = parameter['Name']
    if not (args.silent): print("%s" % (delete_env_name))
    SSM.delete_parameter(Name = delete_env_name)

for parameter in parameters():
    put_env_vars(parameter)
    if args.delete:
        delete_env_vars(parameter)
    