# Copy Parameters #

## Command Reference ##

python copyParameters.py [args]

### Arguments ###

--delete Old-Path New-Path	Delete old parameters after copying
--copy Old-Path New-Path	Copy and preserve old parameters
-s, --silent			Run without printing parameter names

